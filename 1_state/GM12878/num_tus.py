import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as ticker

num=23
data = np.genfromtxt('hg19.chrom.sizes.mod.dat')

file=open("hg19.chrom.sizes.tunum.dat", "w")
for i in range(0, num):
    data_pe = np.genfromtxt('chr_%d/promenh.dat' %(i+1))
    
    file.write("%d    %f    %d\n" %(i+1, data[i,1], len(np.where(data_pe[:,1]==1)[0])))

file.close()    
