import numpy as np
import matplotlib.pyplot as plt

nbead=xxxlenchrxxx
resolution=xxxresolutionxxx

chrstate=np.zeros(nbead)
key=np.zeros(nbead)
statesize=np.zeros(nbead)

#Read enhancer regions
file=np.genfromtxt('chrxxxchrxxx.enh.dat')
len_file=int(len(file))
for i in range(0, len_file):
    i1=file[i,0]
    i2=file[i,1]
    
    index1=int(i1/resolution+1)
    if index1%resolution==0:
        index1=index1-1
    index2=int(i2/resolution+1)
    if index2%resolution==0:
        index2=index2-1
        
    chrstate[index1]=1
    statesize[index1]=(i2-i1)/resolution
    
    chrstate[index2]=1
    statesize[index2]=(i2-i1)/resolution
    
#Read promoters regions
file=np.genfromtxt('chrxxxchrxxx.prom.dat')
len_file=int(len(file))
for i in range(0, len_file):
    i1=file[i,0]
    i2=file[i,1]
    
    index1=int(i1/resolution+1)
    if index1%resolution==0:
        index1=index1-1
    index2=int(i2/resolution+1)
    if index2%resolution==0:
        index2=index2-1
        
    chrstate[index1]=2
    statesize[index1]=(i2-i1)/resolution
    
    chrstate[index2]=2
    statesize[index2]=(i2-i1)/resolution
    
#Read DHS regions
file=np.genfromtxt('key.dat')
len_file=int(len(file))
for i in range(0, len_file):
    i1=file[i,0]
    i2=file[i,1]
    
    index1=int(i1/resolution+1)
    if index1%resolution==0:
        index1=index1-1
    index2=int(i2/resolution+1)
    if index2%resolution==0:
        index2=index2-1
        
    key[index1]=1
    key[index2]=1
    
#Color chain regions
file=open("promenh.dat", "w")
for i in range(0, nbead):
    if key[i]==1:
        file.write("%d    %d    %f\n" %(i, key[i], statesize[i]))
file.close()

    