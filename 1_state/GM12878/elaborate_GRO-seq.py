import scipy.stats
import numpy as np
import matplotlib.pyplot as plt

resolution=3000                          #resolution of the polymer bead
len_polymer=int(xxxlenchrxxx)                   #length of the polymer
beads_data=np.zeros(len_polymer)         #Rescaled experimental data
count=0                                  #Service counter
replicas=1                               #Number of experimantal replicas

for ii in range(0, replicas):
#    print("Replica n.%d" %(ii+1))
    data = np.genfromtxt('GM12878_chrxxxchrxxx.dat')   
    len_data=int(len(data))
    
    #Sum data for each bead
#    print("Sum data")
    for i in range(0,len_data):
        ind1=int(data[i,4])
        ind2=int(data[i,5])
        
        if ind1==ind2:
            beads_data[ind1-1]+=data[i,3]
        else:
            left=data[i,1]/resolution
            right=data[i,2]/resolution
            start=int(left)
            end=int(right)
            
            p1=np.abs(left-end)
            p2=np.abs(right-end)
            interval=p1+p2
            prob1=p1/interval
            prob2=p2/interval
            
            beads_data[ind1-1]+=data[i,3]*prob1
            beads_data[ind2-1]+=data[i,3]*prob2

print("Save data")
file=open("gro-seq_chrxxxchrxxx_gm12878.dat", "w")
for kk in range(0,len_polymer):
    file.write("%d    %f\n" %(kk+1,beads_data[kk]))
file.close()










        
