import numpy as np
import matplotlib.pyplot as plt
import scipy.stats


#Evaluate chromosomes lenghth
def chr_len(num_chr, len_chr, resolution, file_name,dtype=(int, int)):
    data=np.genfromtxt('%s' %file_name)
    
    real_lengths=data[:,1]

    real_lengths=np.array(real_lengths)
    
    for i in range(0, num_chr):
        len_chr[i]=(real_lengths[i]/resolution)+1
    
    return len_chr

#Import data and convert from structured array to array
def import_strarray(chrom, file_name):
    
    #Import data
    data = np.genfromtxt('%s' %file_name, dtype=(int, int, int))
    len_data=int(len(data))
    
    #Assign imported data to array
    vec_data=np.zeros((len_data,3))
    for i in range(0,len_data):
        for j in range(0,3):
            vec_data[i,j]=data[i][j]
    return vec_data, int(len_data)

#Colouring
def colouring(data, len_data, resolution, chain, ind):
    
    #Resolution bounds
    for i in range(0,len_data): 
        left=data[i,1]/resolution
        right=data[i,2]/resolution
        
        ind1=int(left)
        ind2=int(right)
        
        #Non-verlapping and overlapping beads assignment
        if ind1==ind2:
            chain[ind1]=ind
        else:
            end=int(right)
                
            p1=np.abs(left-end)
            p2=np.abs(right-end)
            
            if p1>p2:
                chain[ind1]=ind
            else:
                chain[ind2]=ind
                
    return chain
    
    
def import_strarray_HMM(chrom, file_name):
        #Import data
        data = np.genfromtxt('%s' %file_name, dtype=(int, int, int,int))
        len_data=int(len(data))
        #print(shape(data))
        #Assign imported data to array
        vec_data=np.zeros((len_data,4))
        for i in range(0,len_data):
            for j in range(0,4):
                vec_data[i,j]=data[i][j]
        return vec_data, int(len_data)
    
#Colouring
def colouring_HMM(data, len_data, resolution, chain):
        
        #Resolution bounds
        for i in range(0,len_data): 
            
            left=data[i,1]/resolution
            right=data[i,2]/resolution
            type=data[i,3]
            
            if type==1: # Active Promoter in HMM File
                type=1
            elif type==4 or type==5: # Strong Enhancer in HMM file
                type=2
            else:   
                type=3
            
            ind1=int(left)
            ind2=int(right)
            
            #Non-overlapping and overlapping beads assignment
            if ind1==ind2:
                chain[ind1]=type
            else:
                end=int(right)
                    
                p1=np.abs(left-end)
                p2=np.abs(right-end)
                
                if p1>p2:
                    chain[ind1]=type
                else:
                    chain[ind2]=type
                    
        return chain
        
        
