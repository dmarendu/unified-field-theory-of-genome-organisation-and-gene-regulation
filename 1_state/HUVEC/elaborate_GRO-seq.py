import scipy.stats
import numpy as np
import matplotlib.pyplot as plt

resolution=3000                          #resolution of the polymer bead
len_polymer=int(xxxlenchrxxx)                   #length of the polymer
beads_data=np.zeros(len_polymer)         #Rescaled experimental data
count=0                                  #Service counter
replicas=1                               #Number of experimantal replicas

for ii in range(0, replicas):
#    print("Replica n.%d" %(ii+1))
#Import experimental data
#    print("Import data")
    data = np.genfromtxt('HUVEC_chrxxxchrxxx_rep3.dat')   #rep3 è quello con correlazione migliore  
    len_data=int(len(data))
    
    #Sum data for each bead
#    print("Sum data")
    for i in range(0,len_data):
        ind1=int(data[i,4])
        ind2=int(data[i,5])
        
        if ind1==ind2:
            beads_data[ind1-1]+=data[i,3]
        else:
            left=data[i,1]/resolution
            right=data[i,2]/resolution
            start=int(left)
            end=int(right)
            
            p1=np.abs(left-end)
            p2=np.abs(right-end)
            interval=p1+p2
            prob1=p1/interval
            prob2=p2/interval
            
            beads_data[ind1-1]+=data[i,3]*prob1
            beads_data[ind2-1]+=data[i,3]*prob2

print("Save data")
file=open("gro-seq_chrxxxchrxxx_huvec.dat", "w")
for kk in range(0,len_polymer):
    file.write("%d    %f\n" %(kk+1,beads_data[kk]))
file.close()

#print("Correlation")
#exp_data1 = np.genfromtxt('GROseq.chr14.res3kbp.allbeads.dat')
#exp_data2 = np.genfromtxt('gro-seq_chr14_huvec.dat')
#corr=scipy.stats.pearsonr(exp_data1[:,1], exp_data2[:,1])
#print("Correlation: %f" %corr[0])



# data1 = np.genfromtxt('HUVEC_chr14_rep1.dat') 
# data2 = np.genfromtxt('HUVEC_chr14_rep2.dat') 
# data3 = np.genfromtxt('HUVEC_chr14_rep3.dat') 

# file1=np.genfromtxt("gro-seq_chr14_huvec_rep1.dat")
# file2=np.genfromtxt("gro-seq_chr14_huvec_rep2.dat")
# file3=np.genfromtxt("gro-seq_chr14_huvec_rep3.dat")

# beads_data_new=np.zeros(len_polymer)
# beads_data_new=(file1[:,1]+file2[:,1]+file3[:,1])/3
# file=open("gro-seq_chr14_huvec_mean.dat", "w")
# for kk in range(0,len_polymer):
#     file.write("%d    %f\n" %(kk+1,beads_data_new[kk]))
# file.close()

# print("Correlation")
# exp_data1 = np.genfromtxt('GROseq.chr14.res3kbp.allbeads.dat')
# exp_data2 = np.genfromtxt('gro-seq_chr14_huvec_mean.dat')
# corr=scipy.stats.pearsonr(exp_data1[:,1], exp_data2[:,1])
# print("Correlation: %f" %corr[0])








        
