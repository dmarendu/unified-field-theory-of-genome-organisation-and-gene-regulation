echo "Beginning"

#Chromosomes we want to simulate, 23=X, 34=Y
declare -a chr=("1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12" "13" "14" "15" "16" "17" "18" "19" "20" "21" "22" "23" "24")
#declare -a chr=("9")
#declare -a chr=("1" "17" "23")

#Polymer chain resolution
declare resolution=3000
echo "Resolution: $resolution"

echo "Gro-seq substitution"
#Substitute X and Y character with 23 and 24 in GRO-seq data file
sed 's~chrX~chr23~g'  gro_seq_H1.bed > gro_seq_H1_mod1.bed
sed 's~chrY~chr24~g'  gro_seq_H1_mod1.bed > gro_seq_H1_mod.bed

echo "cbpeaks substitution"
#Substitute X and Y character with 23 and 24 in cbpeaks data file
sed 's~chrX~chr23~g' cbpeaks_DNase.hESC.bed > cbpeaks_DNase.hESC_mod1.bed
sed 's~chrY~chr24~g' cbpeaks_DNase.hESC_mod1.bed > cbpeaks_DNase.hESC_mod.bed

echo "HMM substitution"
#Substitute X and Y character with 23 and 24 in HMM data file
sed 's~chrX~chr23~g' wgEncodeBroadHmmH1hescHMM.bed > wgEncodeBroadHmmH1hescHMM_mod1.bed
sed 's~chrY~chr24~g' wgEncodeBroadHmmH1hescHMM_mod1.bed > wgEncodeBroadHmmH1hescHMM_mod.bed


echo "Begin cycle over chromosomes"
#Begin cycle over all chromosomes
for i in "${chr[@]}"
do

echo "Chromosome $i"

mkdir -p chr_${i}

#Resolved chromosome length
lenchr=$(awk -v chromo="${i}" -v res="${resolution}" '{if($1==chromo) print int($2/res+1)}' hg19.chrom.sizes.mod.dat)
echo "Resolved chromosome length: ${lenchr}"

#Resolve GRO-seq
echo "Resolve GRO-seq"
##########################
cp elaborate_GRO-seq.py chr_${i}/
cp gro_seq_H1_mod.bed  chr_${i}/
cd chr_${i}
awk -v chromo="${i}" -v chrchromo="chr${i}" -v res="${resolution}"  '{if($1==chrchromo) print $1,$2,$3,$4,int($2/res+1),int($3/res+1)}'  gro_seq_H1_mod.bed > H1hesc_chr${i}.dat
sed -i '.bak' "s/xxxresolutionxxx/${resolution}/g" elaborate_GRO-seq.py
sed -i '.bak' "s/xxxchrxxx/${i}/g" elaborate_GRO-seq.py
sed -i '.bak' "s/xxxlenchrxxx/${lenchr}/g" elaborate_GRO-seq.py
python3 elaborate_GRO-seq.py
cd ..
##############################

echo "Find peaks"
#Save cbpeaks peak position in key.dat
awk -v chromo="${i}" -v chrchromo="chr${i}" '{if($1==chrchromo) print $2,$3}' cbpeaks_DNase.hESC_mod.bed > key.dat

#Save HMM peak position in HMM_chr.dat
awk -v chromo="${i}" -v chrchromo="chr${i}" '{if($1==chrchromo) print chromo,$2,$3,($4+0)}' wgEncodeBroadHmmH1hescHMM_mod.bed > HMM_chr.dat

#Distinguish between promoters and enhancers in HMM_chr.dat
awk -v chromo="${i}" -v chrchromo="chr${i}" '{if($4==1)  print $2,$3}' HMM_chr.dat > chr${i}.prom.dat
awk -v chromo="${i}" -v chrchromo="chr${i}" '{if($4==4 || $4==5)  print $2,$3}' HMM_chr.dat > chr${i}.enh.dat

echo "Copy file and script in folder"
mv key.dat chr_${i}/
mv HMM_chr.dat chr_${i}/
mv chr${i}.prom.dat chr_${i}/
mv chr${i}.enh.dat chr_${i}/

cp identifypromenh.f chr_${i}/
cp transcription_states.py chr_${i}/
cp hg19.chrom.sizes.mod.dat chr_${i}/
cp transcrition_states_functions_new.py chr_${i}/
#cp gro-seq_chr${i}_huvec.dat chr_${i}/

cd chr_${i}/
#cp transcription_states_new.py transcription_states_${i}.py
sed -i '.bak' "s/xxxresolutionxxx/${resolution}/g" identifypromenh.f
sed -i '.bak' "s/xxxchrxxx/${i}/g" identifypromenh.f
sed -i '.bak' "s/xxxlenchrxxx/${lenchr}/g" identifypromenh.f

sed -i '.bak' "s/xxxresolutionxxx/${resolution}/g" transcription_states.py
sed -i '.bak' "s/xxxchrxxx/${i}/g" transcription_states.py

echo "Identify promoters and enhancers"
gfortran -o exe -Ofast identifypromenh.f
./exe > dump_ch${i}

echo "Run main script (python)"
python3 transcription_states.py

echo "Cleaning"
rm -r __pycache__ 
#rm dump_chr${i}
#rm a.out
rm elaborate_GRO-seq.py
rm elaborate_GRO-seq.py.bak
#rm identifypromenh.f
rm transcription_states.py
rm transcrition_states_functions_new.py
rm transcription_states.py.bak
#rm chr${i}.enh.dat
#rm chr${i}.prom.dat
#rm key.dat
#rm promenh.dat
rm hg19.chrom.sizes.mod.dat
rm HMM_chr.dat
rm H1hesc_chr${i}.dat

cd ..

done

echo "End cycle"

rm gro_seq_H1_mod1.bed
rm gro_seq_H1_mod.bed
rm cbpeaks_DNase.hESC_mod1.bed
rm cbpeaks_DNase.hESC_mod.bed
rm wgEncodeBroadHmmH1hescHMM_mod1.bed
rm wgEncodeBroadHmmH1hescHMM_mod.bed

echo "Plot Spearman"
#python3 plot_sperman.py

echo "End"

