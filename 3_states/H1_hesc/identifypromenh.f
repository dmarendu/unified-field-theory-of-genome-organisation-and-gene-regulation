      program promotersenhancers
      implicit none
      integer*4 nbeadmax
      parameter(nbeadmax=100000)
      integer*4 nbead,resolution
      integer*4 i,i1,i2,index1,index2
      integer*4 chrstate(nbeadmax)
      integer*4 key(nbeadmax)
      double precision statesize(nbeadmax)

      nbead=xxxlenchrxxx
      resolution=xxxresolutionxxx

      do i=1,nbeadmax
         chrstate(i)=0 !neither promoter nor enhancer
         key(i)=0
      enddo

ccccc Read enhancers regions cccccccccccccccccccccccccc
      open(unit=2,file='chrxxxchrxxx.enh.dat',status='unknown')
 5    read(2,*,end=10) i1,i2
      index1=int(i1/resolution)+1
      if(mod(i1,resolution).eq.0) index1=index1-1
      index2=int(i2/resolution)+1
      if(mod(i2,resolution).eq.0) index2=index2-1
      do i=index1,index2
         chrstate(i)=1 !enhancer
         statesize(i)=float(i2-i1)/resolution
      enddo
      goto 5
 10   close(2)



ccccc Read promoters regions ccccccccccccccccccccccccccc
      open(unit=3,file='chrxxxchrxxx.prom.dat',status='unknown')
 15   read(3,*,end=20) i1,i2
      index1=int(i1/resolution)+1
      if(mod(i1,resolution).eq.0) index1=index1-1
      index2=int(i2/resolution)+1
      if(mod(i2,resolution).eq.0) index2=index2-1
      do i=index1,index2
         chrstate(i)=2 !promoter
         statesize(i)=float(i2-i1)/resolution
      enddo
      goto 15
 20   close(3)


ccccc Read DHS regions ccccccccccccccccccccccccccc
      open(unit=7,file='key.dat',status='unknown')
 25   read(7,*,end=30) i1,i2
      index1=int(i1/resolution)+1
      if(mod(i1,resolution).eq.0) index1=index1-1
      index2=int(i2/resolution)+1
      if(mod(i2,resolution).eq.0) index2=index2-1
      do i=index1,index2
         key(i)=1 !DHS peaks
      enddo
      goto 25
 30   close(7)


ccccc Color Chain regions ccccccccccccccccccccccccccc
      open(unit=10,file='promenh.dat',status='unknown')
      do i=1,nbead
         write(6,*) i,chrstate(i),statesize(i)
         if(key(i).eq.1) then
            write(10,*) i,chrstate(i),statesize(i)
         endif
      enddo
      close(10)
         
      stop
      end
