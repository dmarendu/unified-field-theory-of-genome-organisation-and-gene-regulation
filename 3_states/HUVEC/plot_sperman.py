# from sklearn import mixture
from scipy import stats, integrate
import matplotlib.pyplot
import matplotlib.mlab
import math
import numpy as np
from pylab import *
from scipy.optimize import leastsq
from scipy.optimize import curve_fit
from scipy.special import erf
# import pandas as pd
# import seaborn as sns
import numpy as np
import numpy 
import matplotlib.ticker as mtick
import matplotlib.pylab as pl
from matplotlib.cm import get_cmap
import matplotlib.ticker as ticker
import random
style.use('fast')

from scipy import stats, integrate
##############################################
#############################################
############################################
######## FUNCTIONS ########################
def fmt(x, pos):
    a, b = '{:.1e}'.format(x).split('e')
    b = int(b)
    return r'${} \times 10^{{{}}}$'.format(a, b)
def random_color():
    rgbl=[255,0,0]
    random.shuffle(rgbl)
    return tuple(rgbl)

def f(x):
    return 3.0*x
def g(x):
    return (x-3.15e-6)**(0.58)
def smooth(x,window_len=11,window='hanning'):
    """smooth the data using a window with requested size.
    
    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal 
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.
    
    input:
        x: the input signal 
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal
        
    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)
    
    see also: 
    
    numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
    scipy.signal.lfilter
 
    TODO: the window parameter could be the window itself if an array instead of a string
    NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.
    """

    if x.ndim != 1:
        raise ValueError( "smooth only accepts 1 dimension arrays.")

    if x.size < window_len:
        raise ValueError("Input vector needs to be bigger than window size.")


    if window_len<3:
        return x


    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError( "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")


    s=numpy.r_[x[window_len-1:0:-1],x,x[-2:-window_len-1:-1]]
    #print(len(s))
    if window == 'flat': #moving average
        w=numpy.ones(window_len,'d')
    else:
        w=eval('numpy.'+window+'(window_len)')

    y=numpy.convolve(w/w.sum(),s,mode='valid')
    return y

##################################################################
##################################################################
#################################################################
################ STYLE ##########################################
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
#style.use('seaborn-pastel')
#sns.set_style("ticks")

#sns.despine()
#sns.set_context("paper")




##################### FOR DIFFERENT COLORS #######################



fig, ax = plt.subplots(figsize=(8,5))
ax.set_ylabel(r'spearman correlation', fontsize=24,rotation='vertical',labelpad=20)
#ax.set_xlabel(r'TU', fontsize=24)
#ax.tick_params(axis="x", labelsize=20)
#ax.tick_params(axis="y", labelsize=20)
#ax.yaxis.offsetText.set_fontsize(20)
#ax.xaxis.offsetText.set_fontsize(20)
#ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
#rects1 = ax.bar(tu_6,activity_sim_6, 80.0, label='Men')
#rects2 = ax.bar(np.array(tu_8)+1,np.array(activity_sim_8)/790,25, alpha=0.6,linestyle='None',color='#FF2276')
#rects3 = ax.bar(np.array(tu_9)+1,np.array(activity_sim_9)/790,25, alpha=0.6,linestyle='None',color='#FFCE00')
#rects4 = ax.bar(np.array(tu_10)+1,np.array(activity_sim_10)/790,25, alpha=0.6,linestyle='None',color='#00F8B3')

#rects2 = ax.scatter(np.array(tu_8)+1,np.array(activity_sim_8)/790,25, alpha=0.6,linestyle='None',color='#FF2276')
#rects3 = ax.scatter(np.array(tu_9)+1,np.array(activity_sim_9)/790,25, alpha=0.6,linestyle='None',color='#FFCE00')
#rects4 = ax.scatter(np.array(tu_10)+1,np.array(activity_sim_10)/790,25, alpha=0.6,linestyle='None',color='#00F8B3')

data = {u'1': 0.643,
    u'2':  0.633,
    u'3':  0.607,
    u'4':  0.618,
    u'5':  0.659,
    u'6':  0.639,
    u'7':  0.662,
    u'8':  0.629,
    u'9':  0.623,
    u'10': 0.634,
    u'11': 0.654,
    u'12': 0.672,
    u'13': 0.628,
    u'14': 0.673,
    u'15': 0.613,
    u'16': 0.642,
    u'17': 0.631,
    u'18': 0.639,
    u'19': 0.623,
    u'20': 0.641,
    u'21': 0.665,
    u'22': 0.641,
    u'X' : 0.632}
names = list(data.keys())
values = list(data.values())
color = iter(cm.rainbow(np.linspace(0, 1, 23)))
for i in range(0,23):
    c=next(color)
    plt.bar(i,values[i],tick_label=names[i],color=c,alpha=0.6,width=0.4)
idx = np.asarray([i for i in range(len(names))])
ax.set_xticks(idx)
ax.set_xticklabels(names, rotation=65)

#plt.xticks(range(1,22),names)

#ax.set_xlim(1,3000)
for _,s in ax.spines.items():
    s.set_linewidth(2.0)
    s.set_color('black')
plt.savefig('spearman.pdf', format='pdf', dpi=600,bbox_inches = 'tight')
plt.savefig('spearman.svg', format='svg', dpi=600,bbox_inches = 'tight')
plt.savefig('spearman.png', format='png', dpi=600,bbox_inches = 'tight', transparent=True)



############################################################
