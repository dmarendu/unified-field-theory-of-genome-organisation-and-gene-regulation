
declare -a chr=("1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12" "13" "14" "15" "16" "17" "18" "19" "20" "21" "22" "23" "24")
#declare -a chr=("1" "8")
#declare -a chr=("1" "17" "23")

declare resolution=3000


sed 's~chrX~chr23~g'  HUVEC_GROseq_normoxia_rep3.bedGraph > HUVEC_GROseq_normoxia_rep3.bedGraph_mod1
sed 's~chrY~chr24~g'  HUVEC_GROseq_normoxia_rep3.bedGraph_mod1 > HUVEC_GROseq_normoxia_rep3.bedGraph_mod


sed 's~chrX~chr23~g' cbpeaks_DNase.HUVEC.dat > cbpeaks_DNase.HUVEC_mod1.dat
sed 's~chrY~chr24~g' cbpeaks_DNase.HUVEC_mod1.dat > cbpeaks_DNase.HUVEC_mod.dat

sed 's~chrX~chr23~g' wgEncodeBroadHmmHuvecHMM.bed > wgEncodeBroadHmmHuvecHMM_mod1.bed
sed 's~chrY~chr24~g' wgEncodeBroadHmmHuvecHMM_mod1.bed > wgEncodeBroadHmmHuvecHMM_mod.bed


for i in "${chr[@]}"
do

mkdir -p chr_${i}


lenchr=$(awk -v chromo="${i}" -v res="${resolution}" '{if($1==chromo) print int($2/res+1)}' hg19.chrom.sizes.mod.dat)
echo ${lenchr}


##########################
cp  elaborate_GRO-seq.py chr_${i}/
cp HUVEC_GROseq_normoxia_rep3.bedGraph_mod  chr_${i}/
cd chr_${i}
awk -v chromo="${i}" -v chrchromo="chr${i}" -v res="${resolution}"  '{if($1==chrchromo) print $1,$2,$3,$4,int($2/res+1),int($3/res+1)}'  HUVEC_GROseq_normoxia_rep3.bedGraph_mod > HUVEC_chr${i}_rep3.dat
sed -i '.bak' "s/xxxchrxxx/${i}/g" elaborate_GRO-seq.py
sed -i '.bak' "s/xxxlenchrxxx/${lenchr}/g" elaborate_GRO-seq.py
python3 elaborate_GRO-seq.py
cd ..
##############################

awk -v chromo="${i}" -v chrchromo="chr${i}" '{if($1==chrchromo) print $2,$3}' cbpeaks_DNase.HUVEC_mod.dat > key.dat

awk   -v chromo="${i}" -v chrchromo="chr${i}" '{if($1==chrchromo)  print chromo,$2,$3,($4+0)}' wgEncodeBroadHmmHuvecHMM_mod.bed > HMM_chr.dat


awk   -v chromo="${i}" -v chrchromo="chr${i}" '{if($4==1)  print $2,$3}' HMM_chr.dat > chr${i}.prom.dat

awk   -v chromo="${i}" -v chrchromo="chr${i}" '{if($4==4 || $4==5)  print $2,$3}' HMM_chr.dat > chr${i}.enh.dat

mv key.dat chr_${i}/
mv HMM_chr.dat chr_${i}/
mv chr${i}.enh.dat chr_${i}/
mv chr${i}.prom.dat chr_${i}/

cp identifypromenh.f chr_${i}/

cp transcription_states.py chr_${i}/

cp hg19.chrom.sizes.mod.dat chr_${i}/

cp transcrition_states_functions_new.py chr_${i}/

#cp gro-seq_chr${i}_huvec.dat chr_${i}/

cd chr_${i}/
#cp transcription_states_new.py transcription_states_${i}.py
sed -i '.bak' "s/xxxchrxxx/${i}/g" identifypromenh.f
sed -i '.bak' "s/xxxlenchrxxx/${lenchr}/g" identifypromenh.f


sed -i '.bak' "s/xxxchrxxx/${i}/g" transcription_states.py



#awk 'NR>1 {if($1=="chr14") print $1,$2,$3,$4, int($2/3000)+1,int($3/3000)+1 }' HUVEC_GROseq_normoxia_rep3.bedGraph > HUVEC_chr14.dat
gfortran identifypromenh.f
./a.out > dump_chr${i}


python3 transcription_states.py
#python3 transcription_states_${i}.py
#sed -i "s/xxxfilexxx/cbpeaks_DNase_chr${i}.dat/g" 

cd ..
done
