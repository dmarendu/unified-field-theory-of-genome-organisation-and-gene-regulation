import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import transcrition_states_functions_new as fun

######### Take the length of the Chromosome ########################################
num_chr=23                          #number of chromosomes
resolution=3000                     #resolution of the chain
len_chr=np.zeros(num_chr)           #list of chromosomes lengths 1-23, X, Y
file_name= 'hg19.chrom.sizes.mod.dat'
len_chr=fun.chr_len(int(num_chr), len_chr, resolution, file_name)
chr=xxxchrxxx
print("chr", chr)

########## Read Labelled Chain #####################################################
filename = 'promenh.dat'
data_cm = np.genfromtxt(filename,skip_header=0)
bead_genes=data_cm[:,0]
chain_genes=data_cm[:,1]
lenchr=int(len(bead_genes))
####################################################################################


########### Take the index of the promoters ########################################
promoters=[]
for i in range(lenchr):
    if chain_genes[i]==2:
        promoters.append(bead_genes[i])
###############################################################################



######### redefine lenchr #######################################################
lenchr=len(chain_genes)
#################################################################################


###### Calculate for each gene the distance from the promoter ################
dist=np.zeros(lenchr)
dist_from_promoter=[]
for i in range(lenchr):
        dist_from_prm_prov=abs(np.array(promoters)-bead_genes[i])
        dist_from_prm_prov=np.sort(dist_from_prm_prov) #sort the distance for each gene from promoter
        if(dist_from_prm_prov[0]!=0): #check if I'm a promoter
            dist_from_promoter.append(dist_from_prm_prov[0])
        else:
            dist_from_promoter.append(dist_from_prm_prov[1])
#############################################################################
        
                            

########### Assign b 3 states ###############################################
b=np.ones(lenchr)
for i in range(lenchr):
    if chain_genes[i]==2:
        b[i]=13.1
    elif chain_genes[i]==1:
        b[i]=3.3
    elif chain_genes[i]==3:
        b[i]=1.0
###########################################################################

######## Probability #####################################################
c=86.0/3
probability=np.zeros(lenchr)
for i in range(lenchr):
    probability[i]=b[i]*(1.0+np.double(c/(dist_from_promoter[i])))
#########################################################################


###### Save Data ########################################################
print("Saving Data")
file1=open("activity_1color_xxxchrxxx_only_genes.dat", "w")
for ii in range(0, lenchr):
        file1.write("%d %d %lf %d\n" %(bead_genes[ii], chain_genes[ii], probability[ii],dist_from_promoter[ii]))
file1.close()
#########################################################################



############# Calculate correlation with gro-seq #######################
print("Calculating Correlation with experimental data")
filename = 'gro-seq_chrxxxchrxxx_huvec.dat'
data_cm = np.genfromtxt(filename,skip_header=0)
tu=data_cm[:,0]
activity=data_cm[:,1]

filename = 'activity_1color_xxxchrxxx_only_genes.dat'
data_cm = np.genfromtxt(filename,skip_header=0)
tu_sim=data_cm[:,0]
tu_sim_type=data_cm[:,1]
activity_sim=data_cm[:,2]

tu_plot=[]
activity_data=[]
activity_data_sim=[]
for j in range(len(tu_sim)):
   for i in range(len(tu)):
        if (tu[i]==tu_sim[j]):
            tu_plot.append(tu[i])
            activity_data.append(activity[i])
            activity_data_sim.append(activity_sim[j])
#### Print Result at video ##############################################
from scipy import stats
coef, p=stats.spearmanr(activity_data, activity_data_sim)
print('Spearmans correlation coefficient one color: %.3f' % coef)
print('p value: %E' % p)
file=open("spearman_chr%d.dat" %chr, "w")
file.write("%d    %f" %(chr, coef))
file.close()
#########################################################################

from scipy.stats import rankdata
groseq_ranked=rankdata(activity_data)
sim_ranked=rankdata(activity_data_sim)
np.savetxt("groseq_sims_ranked_xxxchrxxx.txt",np.array([groseq_ranked,sim_ranked]).T)
x=groseq_ranked
y=sim_ranked
plt.scatter(sim_ranked,groseq_ranked)
plt.hist2d(sim_ranked,groseq_ranked,bins=5,cmap='plasma', vmin=60,vmax=225)
cb = plt.colorbar(label='count in bin')
cb.set_label('counts in bin')
plt.savefig("ranked_plot_counts_xxxchrxxx.png")
#plt.show()

plt.clf()
############# Correlation Plot ############################################
from scipy.stats import gaussian_kde
data = np.vstack([x, y])
kde = gaussian_kde(data)
        # evaluate on a regular grid
xgrid = np.linspace(min(x), max(x), 10)
ygrid = np.linspace(min(y), max(y), 10)
Xgrid, Ygrid = np.meshgrid(xgrid, ygrid)
Z = kde.evaluate(np.vstack([Xgrid.ravel(), Ygrid.ravel()]))

        # Plot the result as an image
plt.imshow(Z.reshape(Xgrid.shape),
                   origin='lower', aspect='auto',
                   extent=[min(x), max(x), min(y), max(y)],
                   cmap='magma')
cb = plt.colorbar()
cb.set_label("density")
plt.savefig("ranked_plot_xxxchrxxx.png")
###########################################################################


import matplotlib.ticker as mtick
import matplotlib.pylab as pl
from matplotlib.cm import get_cmap
import matplotlib.ticker as ticker
import matplotlib.pyplot
import matplotlib.mlab
from matplotlib.pyplot import *

fig, (ax) = plt.subplots(nrows=2, ncols=1, figsize=(5, 4))
subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.3, hspace=None)#rects1 = ax.bar(tu_6,activity_sim_6, 80.0, label='Men')
rects2 = ax[0].bar(tu_plot,np.array(sim_ranked), 80.0, label='sim',alpha=0.4)
rects3 = ax[1].bar(tu_plot,np.array(groseq_ranked), 80.0, label='exp',alpha=0.4, color='red')
plt.savefig('activity_xxxchrxxx.pdf', format='pdf', dpi=600,bbox_inches = 'tight')
plt.savefig('activity_xxxchrxxx.svg', format='svg', dpi=600,bbox_inches = 'tight')
plt.savefig('activity_xxxchrxxx.png', format='png', dpi=600,bbox_inches = 'tight', transparent=True)
