import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import matplotlib.ticker as ticker

#Collect data
num_chr=23
chrs=np.zeros(num_chr)
corr=np.zeros(num_chr)
file=open("spearman_all.dat", "w")
for i in range(0, num_chr):
    data_cm = np.genfromtxt('chr_%d/spearman_chr%d.dat' %(i+1, i+1),skip_header=0)
    chrs[i]=data_cm[0]
    corr[i]=data_cm[1]
    file.write("%d    %f\n" %(chrs[i], corr[i]))
file.close()
    
    
names=["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "X"]



fig, ax = plt.subplots(figsize=(8,5))
ax.set_ylabel('spearman correlation', fontsize=16,rotation='vertical',labelpad=20)
# ax.set_xlabel('bead index', fontsize=16)
ax.tick_params(axis="x", labelsize=12)
ax.tick_params(axis="y", labelsize=12)
ax.yaxis.offsetText.set_fontsize(20)
ax.xaxis.offsetText.set_fontsize(20)
# ax.set_ylim([0,0.3])
ax.ticklabel_format(style='plain', axis='y', scilimits=(0,0))
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.set_xticks(chrs)
ax.set_xticklabels(names)

x_maj=1
y_maj=0.2
x_min=1
y_min=0.1

ax.yaxis.set_major_locator(ticker.MultipleLocator(y_maj))
ax.yaxis.set_minor_locator(ticker.MultipleLocator(y_min))

dim_bars=25
dim_points=16

ax.bar(chrs,corr, alpha=0.6,linestyle='None',color='red')


plt.savefig('spearman.pdf', format='pdf', dpi=600,bbox_inches = 'tight')




############################################################
