# Unified-Field Theory of Genome Organisation and Gene Regulation

This repository contains datafiles and essential codes associated with the paper "A Unified-field theory of genome organization and gene regulation", by G. Negro, M. Semeraro, P. R. Cook and D. Marenduzzo. 

Folders contain data and scripts to reproduce the results regarding the 1 state and 3 states formula, for all the cell lines considered in the paper. Results can be reproduced running the main script pipe.sh within each folder. The calculation of the transcription probability is implemented in python, in the transcription_states.py scripts, and these are called by the main .sh script.   
